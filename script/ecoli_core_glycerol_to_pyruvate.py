# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
from os import path
import pandas as pd
from cobra.io import read_sbml_model
import argparse
from multiprocessing import Pool

from fermentation_pathways.path import Path
from fermentation_pathways import REC_PATH


def find_glycolysis_alternatives(
        n_atp: int,
        max_iterations: int,
        experiment_name: str
) -> None:
    """Find glycolysis alternative pathways using path-designer."""
    logging.info(f"Looking for pathways from glycerol to pyruvate that make "
                 f"exactly {n_atp} ATPs. Stop after {max_iterations} "
                 f"solutions.")

    cobra_model = read_sbml_model(path.join(REC_PATH, "e_coli_core.xml"))
    
    for reaction_id in ["ATPM", "ATPS4r", "EX_glc__D_e", "BIOMASS_Ecoli_core_w_GAM"]:
        cobra_model.reactions.get_by_id(reaction_id).knock_out()
        
    bounds_df = pd.read_csv(path.join(REC_PATH, f"bounds_{cobra_model.id}.csv"))
    cf = Path(cobra_model)

    cf.set_bounds(bounds_df)
    
    cf.set_pathway_objective(
        {"dhap_c": -1, "adp_c": -n_atp, "pi_c": -n_atp, "pyr_c": 1, "atp_c": n_atp}
    )
    
    for met in ["h2o_c", "atp_c", "adp_c", "amp_c", "pi_c", "h_c", "h_e"]:
        cf.add_currency_metabolite(met)

    cf.merge_metabolites("e_acceptor",
                         ["nad_c", "nadp_c", "q8_c"])
    cf.merge_metabolites("e_donor",
                         ["nadh_c", "nadph_c", "q8h2_c"])
    cf.add_reaction(id="__regenerate_electron_carrier__",
                    name="Regernerate Electron Carrier",
                    metabolites_to_add={"e_acceptor": -1, "e_donor": 1},
                    lower_bound=-1000,
                    upper_bound=1000,
                    objective_coefficient=0.0)

    cf.add_reaction(id="__regenerate_glutamate__",
                    name="Regenerate Glutamate",
                    metabolites_to_add={"akg_c": -1, "glu__L_c": 1},
                    lower_bound=0,
                    upper_bound=1000,
                    objective_coefficient=0.0)

    cf.remove_metabolites(["h2o_c", "h_c", "h_e"])
    for free_met in ["pi_c", "co2_c", "o2_c", "nh4_c"]:
        cf.add_reaction(id=f"__regenerate_{free_met}__",
                        name=f"Regenerate {free_met}",
                        metabolites_to_add={free_met: 1},
                        lower_bound=-1000,
                        upper_bound=1000,
                        objective_coefficient=0.0)

    cf.add_cofactors(["adp_c", "atp_c", "e_acceptor", "e_donor",
                      "co2_c", "o2_c", "pi_c", "nh4_c", "glu__L_c", "akg_c"])
    
    cf.build_stoichiometric_matrix()

    cf.search(
        f"{experiment_name}_{n_atp}atp",
        max_iterations=max_iterations,
        output_format="both",
    )



parser = argparse.ArgumentParser(description='Glycolysis pathway designer')
parser.add_argument(
    '--iter', type=int,
    help='the maximum number of solutions to show got each ATP yield.',
    default=20
)
parser.add_argument(
    '--num_processes', type=int,
    help='the maximum number of processes to use in parallel.',
    default=1
)
parser.add_argument(
    '--min_atp', type=int,
    help='the minimum number of ATP to consider.',
    default=0
)
parser.add_argument(
    '--max_atp', type=int,
    help='the maximum number of ATP to consider.',
    default=0
)
parser.add_argument(
    '--debug', action="store_true",
    help='flag for turning on the debug mode.'
)
parser.add_argument(
    '--experiment_name', type=str,
    help='the name to use for writing the results.',
    default=path.basename(__file__).replace('.py', '')
)
args = parser.parse_args()
if args.debug:
    logging.getLogger().setLevel(logging.DEBUG)
else:
    logging.getLogger().setLevel(logging.INFO)

args_list = [(i, int(args.iter), args.experiment_name)
             for i in range(args.min_atp, args.max_atp+1)]

if len(args_list) == 1 or args.num_processes == 1:
    for args in args_list:
        find_glycolysis_alternatives(*args)
else:
    with Pool(args.num_processes) as p:
        p.starmap(find_glycolysis_alternatives, args_list)
