# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""A script for finding all possible pathways from A to B."""
import logging
from typing import Dict, Iterable, List, Set, Tuple, Union

import cobra
import cvxpy as cp
import numpy as np
import pandas as pd
from cvxpy.constraints.constraint import Constraint
from equilibrator_api import Q_, ComponentContribution
from sbtab.SBtab import SBtabDocument

from . import util
from .solution import Solution


class Path(object):
    """Finds pathways using the Universal stoichiometric matrix."""

    DEFAULT_LB = 1e-6
    DEFAULT_UB = 1e-2
    LARGE_ENERGY_VALUE = 1e3

    def __init__(self, cobra_model: cobra.Model):
        """Create a Path for designing pathways based on goal stoichiometry.

        :param cobra_model: a COBRA model, from which to take the list of
        reactions.
        """
        self.cobra_model = cobra_model.copy()
        self.comp_contrib = ComponentContribution()
        self.bounds_df = dict()

        self.cofactor_set: Set[cobra.Metabolite] = set()
        self.lp_model = None
        self.__sum_of_fluxes_ub = 100.0
        self.__mdf_lb = Q_("0.0 kJ/mol")
        self._objective_coefficients: Dict[str, float] = dict()
        self._currency_metabolites = set()
        logging.debug("Setting the objective coefficients of all reactions " "to 1")

        self.standard_dg = util.calculate_standard_dgs(
            self.cobra_model, self.comp_contrib
        )
        self.S = None
        self.reaction_df = None

    def set_bounds(self, bounds_df: pd.DataFrame) -> None:
        for row in bounds_df.itertuples():
            met = self.cobra_model.metabolites.get_by_id(row.bigg_id)
            self.bounds_df[met] = (row.lb, row.ub)

    def build_stoichiometric_matrix(self) -> None:
        """Build the stoichiometric matrix.
        
        where reversible reactions are split into to irreversible ones).
        """
        S = []
        irreversible_reaction = []
        for rxn in self.reactions:
            self.set_objective_coefficient(rxn, 1.0)

            # Split this reaction to forward and backward
            if rxn.upper_bound > 0:
                irreversible_reaction.append(
                    (
                        rxn,
                        rxn.id,
                        rxn.id + "_F",
                        1,
                        max(rxn.lower_bound, 0),
                        rxn.upper_bound,
                    )
                )
                S_col = pd.Series(index=self.metabolites)
                for met in rxn.metabolites:
                    S_col[self.metabolites] = rxn.get_coefficient(met)
                S.append(S_col)

            if rxn.lower_bound < 0:
                # invert all the bounds and stoichiometric coefficients
                # to make this into a reaction with a positive flux (going
                # in the reverse direction).
                irreversible_reaction.append(
                    (
                        rxn,
                        rxn.id,
                        rxn.id + "_R",
                        -1,
                        max(-rxn.upper_bound, 0),
                        -rxn.lower_bound,
                    )
                )
                S_col = pd.Series(index=self.metabolites)
                for met in rxn.metabolites:
                    S_col[self.metabolites] = -rxn.get_coefficient(met)
                S.append(S_col)

        self.S = pd.DataFrame(S).T
        self.reaction_df = pd.DataFrame(
            data=irreversible_reaction,
            columns=[
                "reaction",
                "original_reaction_id",
                "reaction_id",
                "direction",
                "lower_bound",
                "upper_bound",
            ],
        )
        self.S.columns = self.reaction_df.reaction_id
        self.S.index = self.metabolites

    def set_objective_coefficient(
        self, rxn: Union[str, cobra.Reaction], coefficient: float
    ) -> None:
        """Set the objective coefficient of a reaction.

        :param rxn: cobra Reaction or Reaction ID
        :param coefficient: the objective coefficient
        """
        assert coefficient >= 0, "Objective coefficients must be positive or 0"
        if type(rxn) == str:
            self._objective_coefficients[rxn] = coefficient
        else:
            self._objective_coefficients[rxn.id] = coefficient

    @property
    def objective_coefficients(self) -> np.ndarray:
        """Get the objective coefficients of all reactions

        :return: the objective coefficients as a vector
        """
        res = []
        for row in self.reaction_df.itertuples():
            res.append(self._objective_coefficients.get(row.original_reaction_id, 0.0))
        return np.array(res, dtype=float)

    def add_currency_metabolite(self, met: Union[str, cobra.Metabolite]) -> None:
        """Add a metabolite to the set of currency metabolites

        :param met: a metabolite
        """
        if type(met) == str:
            self._currency_metabolites.add(self.cobra_model.metabolites.get_by_id(met))
        else:
            self._currency_metabolites.add(met)

    @property
    def metabolites(self) -> Iterable[cobra.Metabolite]:
        """Return the list of COBRA metabolites in the model."""
        return self.cobra_model.metabolites

    @property
    def reactions(self) -> Iterable[cobra.Reaction]:
        """Return the list of COBRA reactions in the model."""

        # First check for duplicate reactions, i.e. reactions that are
        # identical in terms of stoichiometry
        rxn_hashes = []
        for rxn in self.cobra_model.reactions:
            rxn_hashes.append((rxn, str(rxn.metabolites)))
        df = pd.DataFrame(data=rxn_hashes, columns=["reaction", "hash"])
        unique_df = df.drop_duplicates(subset="hash", keep="first")

        return unique_df.reaction.values

    def get_currency_metabolites(self) -> Set[cobra.Metabolite]:
        """Get the set of currency metabolites (used to define type II
        cycles)."""
        return self.__currency_metabolites

    def set_currency_metabolites(self, x: Set[cobra.Metabolite]):
        """Set the set of currency metabolites (used to define type II
        cycles)."""
        self.__currency_metabolites = x

    def add_reaction(
        self,
        id: str,
        name: str,
        metabolites_to_add: Dict[Union[cobra.Metabolite, str], float],
        lower_bound: float = -1000.0,
        upper_bound: float = 1000.0,
        objective_coefficient: float = 1.0,
    ):
        """Insert (or override) a reaction to the COBRA model."""
        logging.debug(f"Adding reaction: {id}")

        if self.cobra_model.reactions.has_id(id):
            self.cobra_model.reactions.get_by_id(id).remove_from_model()
        rxn = cobra.Reaction(
            id=id, name=name, lower_bound=lower_bound, upper_bound=upper_bound
        )
        self.cobra_model.add_reaction(rxn)
        rxn.add_metabolites(metabolites_to_add)
        self.set_objective_coefficient(rxn, objective_coefficient)
        return rxn

    def set_pathway_objective(
        self, metabolites_to_add: Dict[Union[cobra.Metabolite, str], float]
    ) -> None:
        """Add a dictionary of the overall reaction required for this path
        search problem."""
        logging.debug("Setting pathway objective")
        self.add_reaction(
            id="__path_objective__",
            name="objective",
            metabolites_to_add=metabolites_to_add,
            lower_bound=-1 - 1e-5,
            upper_bound=-1 + 1e-5,
            objective_coefficient=0.0,
        )

    @property
    def mdf_lb(self) -> Q_:
        """Get the lower bound on the MDF."""
        return self.__mdf_lb

    @mdf_lb.setter
    def mdf_lb(self, lb: Q_) -> None:
        """Set the lower bound for the MDF. The default is 0."""
        self.__mdf_lb = lb

    @property
    def sum_of_fluxes_ub(self) -> float:
        """Get the lower bound on the MDF."""
        return self.__sum_of_fluxes_ub

    @sum_of_fluxes_ub.setter
    def sum_of_fluxes_ub(self, ub: float) -> None:
        """Set the lower bound for the MDF. The default is 0."""
        self.__sum_of_fluxes_ub = ub

    def merge_metabolites(
        self,
        new_metabolite_name: str,
        old_metabolites: Iterable[Union[cobra.Metabolite, str]],
    ):
        """Merge a list of metabolites into one. This helps in deduping
        reactions later."""

        group_met = cobra.Metabolite(id=new_metabolite_name)
        self.cobra_model.add_metabolites(group_met)
        for met in old_metabolites:
            if type(met) == str:
                met = self.cobra_model.metabolites.get_by_id(met)
            for rxn in met.reactions:
                rxn.add_metabolites({group_met: rxn.get_coefficient(met)})
            self.cobra_model.remove_metabolites(met)

    def remove_metabolites(
        self, metabolites: Iterable[Union[cobra.Metabolite, str]]
    ) -> None:
        """Remove metabolites from the model."""
        for met in metabolites:
            if type(met) == str:
                met = self.cobra_model.metabolites.get_by_id(met)
            self.cobra_model.remove_metabolites(met)

    def add_cofactors(self, cofactors: Iterable[Union[cobra.Metabolite, str]]) -> None:
        """Add metabolites to the set of co-factors (only affects the
        pathway visualization)."""
        for c in cofactors:
            if type(c) == str:
                c = self.cobra_model.metabolites.get_by_id(c)
            self.cofactor_set.add(c)

    def _create_min_flux_lp(self) -> cp.Problem:
        """This is only for debugging purposes."""
        assert (
            self.reaction_df is not None
        ), "First, you must run build_stoichiometric_matrix()"

        flux = cp.Variable(shape=self.reaction_df.shape[0], name="flux")
        constraints = [
            flux >= self.reaction_df.lower_bound,
            flux <= self.reaction_df.upper_bound,
            self.S.values @ flux == 0,
        ]

        logging.debug("Creating CVXPY problem: minimize the sum of fluxes")
        return cp.Problem(cp.Minimize(flux @ self.objective_coefficients), constraints)

    def create_min_step_lp(self) -> cp.Problem:
        """Create the LP for finding the path with min number of reactions."""
        assert (
            self.reaction_df is not None
        ), "First, you must run build_stoichiometric_matrix()"

        constraints = []

        logging.debug("Creating flux variables")
        flux = cp.Variable(shape=self.reaction_df.shape[0], name="flux")

        logging.debug("Adding flux lower/upper bounds")
        constraints += [
            flux >= self.reaction_df.lower_bound,
            flux <= self.reaction_df.upper_bound,
        ]

        logging.debug("Adding mass-balance constraints")
        constraints += [
            self.S.values @ flux == 0,
        ]

        logging.debug("Creating flux indicator (boolean) variables")
        gamma = cp.Variable(shape=self.reaction_df.shape[0], name="gamma", integer=True)
        constraints += [0 <= gamma, gamma <= 1]
        M = self.reaction_df.upper_bound.max()

        # Make each gamma into a flux indicator
        # (i.e. so that if v_i > 0 then gamma_i must be equal to 1).
        # We use the following constraint:
        #          - M - 1 <= v_i - M * gamma_i <= 0
        logging.debug("Adding flux indicator constraints")
        constraints += [-M - 1 <= flux - M * gamma, flux - M * gamma <= 0]

        logging.debug(
            f"Adding upper bound for the sum of fluxes: {self.sum_of_fluxes_ub}"
        )
        constraints += [flux @ self.objective_coefficients <= self.sum_of_fluxes_ub]

        if self._currency_metabolites:
            logging.debug("Adding loop-less constraints for currency metabolites")
            loopless_constraints = self._add_loopless_constraints(gamma)
            constraints += loopless_constraints

        if self.comp_contrib is not None:
            logging.debug("Adding Minimum Driving Force constraints")
            margin, mdf_constraints = self._add_optmdf_constraints(gamma)
            constraints += mdf_constraints
            logging.debug("Creating objective: max-MDF and minimum sum of indicators")
            objective = cp.Minimize(-margin + gamma @ self.objective_coefficients)
        else:
            logging.debug("Creating objective: minimum sum of indicators")
            objective = cp.Minimize(gamma @ self.objective_coefficients)

        logging.debug("Creating CVXPY problem")
        return cp.Problem(objective, constraints)

    def _add_loopless_constraints(self, gamma: cp.Variable) -> List[Constraint]:
        """Constraints that remove type II futile cycles.

        In other words, loops that only cycle currency metabolites. To
        achieve this, we create a set of metabolite potential variables and
        add them to the big DataFrame.

        :param stoich_df: a DataFrame containing all variables and the
        stoichiometry
        """
        Nc = len(self.metabolites)
        ln_conc = cp.Variable(shape=(Nc,), name="loopless")

        ln_conc_lb = np.ones(Nc) * -1e3
        ln_conc_ub = np.ones(Nc) * 1e3

        for i, met in enumerate(self.metabolites):
            if met in self._currency_metabolites:
                ln_conc_lb[i] = -1e-3
                ln_conc_ub[i] = 1e-3

        loopless_constraints = [
            ln_conc_lb <= ln_conc,
            ln_conc <= ln_conc_ub,
            1e-3 <= -self.S.T.values @ ln_conc + self.LARGE_ENERGY_VALUE * (1.0 - gamma),
        ]

        return loopless_constraints

    def _add_optmdf_constraints(
        self, gamma: cp.Variable,
    ) -> Tuple[cp.Variable, List[Constraint]]:
        """Add the thermodynamic constraints (TFA)

        :param stoich_df: a DataFrame containing all variables and the
        stoichiometry
        """

        # create the ln concentration variables, and give them the proper
        # bounds
        # create the ln concentration variables, and give them the proper
        # bounds

        logging.debug("Defining log-concentration variables")

        Nc = len(self.metabolites)
        ln_conc = cp.Variable(shape=(Nc,), name="lnC")

        conc_lb = np.ones(Nc) * self.DEFAULT_LB
        conc_ub = np.ones(Nc) * self.DEFAULT_UB

        for i, met in enumerate(self.metabolites):
            if met in self.bounds_df:
                conc_lb[i] = self.bounds_df[met][0]
                conc_ub[i] = self.bounds_df[met][1]

        mdf_constraints = [
            np.log(conc_lb) <= ln_conc,
            ln_conc <= np.log(conc_ub)
        ]

        # The precalculated dG'0 values are in units of RT. We also ignore
        # all those that have a high uncertainty.
        # TODO: use the dG'0 uncertainty matrix in the MDF formulation
        thermo_df = self.standard_dg[
            ~pd.isnull(self.standard_dg.standard_dg_prime)
            & (self.standard_dg.standard_dg_prime_error < 1e3)
        ]

        fwd_df = thermo_df.copy()
        fwd_df["reaction_id"] = thermo_df.bigg_id + "_F"
        rev_df = thermo_df.copy()
        rev_df["reaction_id"] = thermo_df.bigg_id + "_R"
        rev_df["standard_dg_prime"] *= -1.0

        thermo_df = pd.concat([fwd_df, rev_df])
        thermo_df = self.reaction_df.join(
            thermo_df.set_index("reaction_id"), on="reaction_id",
            how="left"
        )
        thermo_df.index = self.reaction_df.reaction_id

        # get the lower bound on the MDF in units of RT (usually 0)
        mdf_lb_over_rt = float((self.mdf_lb / self.comp_contrib.RT).magnitude)
        margin = cp.Variable(shape=1, name="Minimal Driving Force")
        mdf_constraints += [mdf_lb_over_rt <= margin, margin <= self.LARGE_ENERGY_VALUE]

        idx_nan = pd.isnull(thermo_df.standard_dg_prime)
        delta_g = (
                thermo_df.standard_dg_prime[~idx_nan].values +
                self.S.loc[:, ~idx_nan].T.values @ ln_conc
        ).flatten()

        # Add the constraints on dG', i.e.:
        #        margin <= -dG'/RT + M * (1-gamma)
        #
        # which translates to:
        #        margin + M*gamma + dG'/RT <= M
        logging.debug("Adding thermodynamic constraints")

        mdf_constraints += [
            margin <= -delta_g + self.LARGE_ENERGY_VALUE * (1.0 - gamma[~idx_nan.values])
        ]

        return margin, mdf_constraints

    def exclude_solution(
            self,
            solution: Solution,
            radius: int = 0
    ) -> None:
        """Exclude a solution from the LP.

        :param solution: the solution to exclude
        :param radius: the radius (distance in number of reactions) around
        this solution that should be excluded. Default is 0, which means we
        exclude only the solution itself.
        :return:
        """

        # for each previous solution, add constraints on the indicator
        # variables, so that that solution will not repeat (i.e. the sum
        # over the previous reaction set must be less than the size of
        # that set).
        logging.debug("Adding constraints: exclude previous solutions")

        sum_of_objective_coefficients = 0.0
        weighted_sum_of_gammas = []
        for var_name in solution.active_indicators:
            gamma = self.lp_model.variables[var_name]
            tokens = re.findall(r"R@gamma@(\w+)_([FR])", var_name)
            assert len(tokens) == 1, var_name
            reaction_id, direction = tokens[0]
            objective_coefficient = self.get_objective_coefficient(reaction_id)
            if objective_coefficient > 0:
                weighted_sum_of_gammas.append(gamma * objective_coefficient)
                sum_of_objective_coefficients += objective_coefficient

        weighted_sum_of_gammas = symbolics.add(weighted_sum_of_gammas)
        self.lp_model.add(
            Constraint(weighted_sum_of_gammas, lb=None,
                        ub=sum_of_objective_coefficients - radius - 0.5,
                        name=f"G@exclude_solution@{solution.solution_id:03d}")
        )

    def get_next_solution(
        self, prob: cp.Problem, counter: int,
    ) -> Union[Solution, None]:
        """Solve the LP and find the next shortest path.

        :return: A solution or None
        """
        logging.debug("Optimizing MILP")
        prob.solve()
        if prob.status in ["infeasible", "unbounded"]:
            return None

        mdf = Q_("0 kJ/mol")
        solution_data = []

        for variable in prob.variables():
            if variable.name() in ["lnC", "loopless"]:
                for met, val in zip(self.metabolites, variable.value):
                    solution_data.append(
                        ("variable", variable.name(), "metabolite", met.id, val)
                    )
            if variable.name() in ["flux", "gamma"]:
                for row, val in zip(self.reaction_df.itertuples(), variable.value):
                    solution_data.append(
                        ("variable", variable.name(), "reaction", row.reaction_id, val)
                    )
            if variable.name() == "MDF_margin":
                mdf = variable.value * self.comp_contrib.RT

        solution_df = pd.DataFrame(
            data=solution_data,
            columns=["lp_type", "lp_name", "bigg_id_type", "bigg_id", "primal"]
        )

        return Solution(counter, self.cobra_model, self.cofactor_set, solution_df, mdf)

    def search(
        self,
        res_path: str,
        max_iterations: int = 1000,
        write_full_solution: bool = False,
        output_format: str = "escher",
        radius: int = 0,
    ) -> List[Tuple[float, float]]:
        """Find all the n-shortest paths that satisfy the constraints.
        :param res_path: the path in which to write the output files (prefix)
        :param max_iterations: maximum number of iterations
        :param write_full_solution: write all variable and constraint primals to
        a file
        :param output_format: visual output format: "escher", "graphviz", "both" or
        "none"
        :param radius: the exclusion radius around older solutions
        :return: a list of (total flux, MDF) pairs for all solutions
        """
        solutions = []
        assert output_format in [
            "escher",
            "graphviz",
            "both",
            "none",
        ], f"Unknown output format: {output_format}"

        logging.debug("Creating the MILP for finding pathways")
        prob = self.create_min_step_lp()

        sbtabdoc = SBtabDocument("pathways", filename=f"{res_path}.tsv")

        # Use iterative MILP to find the n-best optimal solutions
        for counter in range(max_iterations):
            solution = self.get_next_solution(prob, counter)

            if solution is None:
                if counter == 0:
                    logging.info("Couldn't find any solutions")
                else:
                    logging.info(f"Couldn't find more than {counter} " f"solutions")
                return solutions

            logging.info(
                f"Found path #{counter:03d} comprising "
                f"{solution.n_reactions} reactions, "
                f"MDF = {solution.mdf:.2g}, "
                f"and a total flux = {solution.total_flux:.2g}"
            )
            solutions.append((solution.total_flux, solution.mdf))

            solution.write_to_sbtab(sbtabdoc)
            sbtabdoc.write()

            if write_full_solution:
                with open(f"{res_path}_{counter:03d}.csv", "w") as fp:
                    solution.df.to_csv(fp)

            if output_format in ["escher", "both"]:
                escher_builder = solution.to_escher()
                escher_builder.save_html(f"{res_path}_{counter:03d}.html")
            if output_format in ["graphviz", "both"]:
                g_dot, _, _ = solution.to_graph()
                g_dot.write_svg(f"{res_path}_{counter:03d}.svg", prog="dot")

            self.exclude_solution(solution, radius=radius)

        logging.info(
            f"Stopping after {max_iterations}, but more solutions " f"might exist"
        )

        return solutions
