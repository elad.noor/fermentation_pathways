"""A package for finding alternative fermentation pathways!"""
import os

relpath = lambda s: os.path.join(os.path.abspath(os.path.dirname(__file__)), s)

REC_PATH = relpath("data")
RES_PATH = relpath("../../res")

if not os.path.exists(RES_PATH):
    os.mkdir(RES_PATH)

from .mdf import MDF
from .path import Path
from .solution import Solution
